const {footerInfo} = require("../config.json")

module.exports = {

    supportEmbed: function supportEmbed(message,client,Discord){
        var supportEmbed = new Discord.MessageEmbed()
        .setAuthor("Wenxi here!",client.users.cache.get('303695993462063106').avatarURL())
        .setColor("#cbeb00")
        .setURL('https://discord.gg/AvQfmvN7eu')
        .setTitle("Discord Invite")
        .setDescription("You can join at my Discord Support Server!\n If you find some error or bug, please tell me! \nDo you have any suggestions?, I will be happy to hear you!")
        .setFooter(footerInfo)
        message.channel.send(supportEmbed);
    },

    donateEmbed: function donateEmbed(message, client, Discord){
        var donateEmbed = new Discord.MessageEmbed()
        .setAuthor("Wenxi here!",client.users.cache.get('303695993462063106').avatarURL())
        .setColor("#cbeb00")
        .setTitle("You really like this bot?")
        .setDescription("If yes, this make me smile <3 \nFeel free to donate me for keep updating and upgrading this bot!")
        .addField("Paypal", "wencitera@gmail.com")
        .addField("Discord", "You can join at my community for talk, suggestions, error or bugs reports! \nhttps://discord.gg/AvQfmvN7eu")
        .setFooter(footerInfo)
        message.channel.send(donateEmbed);
    }
}