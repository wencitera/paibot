const Discord = require('discord.js');
const client = new Discord.Client();
const hints = require("./messages/message.json")
const cmd = require("./messages/aliases.json")
const { token } = require("./token.json")
const { prefix, log_channel, develope_guild } = require("./config")
const { findCharacter, findWeapon, findArtifact, findTalentMaterials, findTalents, findConstelations, findWeaponMaterials } = require("./functions/findFunctions")
const { remindToUser, removeRemindToUser, remindListOfUser } = require("./functions/remindFunctions")
const { sendDailyAnnouncementChannel, sendTodayAnnouncement } = require("./functions/dailyFunctions")
const { newDatabase, setAnnouncementChannel, removeAnnouncementChannel } = require("./database/db")

var cron = require('node-cron');
const { listCommands, listWeapons, listCharacters, listSetArtifact } = require('./functions/listFunctions');
const { supportEmbed, donateEmbed } = require('./embeds/gestionEmbeds');
var dsv;

function createDatabase(guild) {
  newDatabase(guild)
  var log = client.guilds.cache.get(develope_guild);
  log.channels.cache.get(log_channel).send("New DB: " + guild.name + " " + guild.id)
}

client.on("guildCreate", guild => {
  createDatabase(guild)
})

client.on('message', message => {

  if (!message.content.startsWith(prefix) || message.author.bot || message.channel.type === 'dm') return;

  const args = message.content.slice(prefix.length).trim().split(' ');
  const command = args.shift().toLowerCase();

  //Mecanismo por si hubo una desconexión del bot y lo invitaron justo
  try {
    dsv = require(`./database/servers/${message.guild.id}`)
  } catch
  {
    createDatabase(message.guild)
  }


  //Comandos sin argumentos
  if (!args.length) {
    if (cmd.helpAlises.includes(command)) {
      listCommands(message, client, Discord)
      return;
    }

    if (cmd.donateAliases.includes(command)) {
      donateEmbed(message, client, Discord)
      return
    }

    if (cmd.supportAliases.includes(command)) {
      supportEmbed(message, client, Discord)
      return;
    }

    if (cmd.listArtifactAliases.includes(command)) {
      listSetArtifact(message, Discord)
      return;
    }

    if (cmd.listWeaponAliases.includes(command)) {
      listWeapons(message, Discord)
      return;
    }

    if (cmd.listCharacterAliases.includes(command)) {
      listCharacters(message, Discord)
      return;
    }

    if (cmd.reminderListAlias.includes(command)) {
      remindListOfUser(message, dsv, Discord)
      return;
    }

    if (cmd.setAnnouncementChannelAliases.includes(command)) {
      setAnnouncementChannel(message, dsv);
      return;
    }

    if (cmd.removeAnnouncementChannelAliases.includes(command)) {
      removeAnnouncementChannel(message, dsv);
      return;
    }

    if (cmd.todayFarmAliases.includes(command)) {
      sendTodayAnnouncement(message, dsv, client, Discord)
      return;
    }

    if (command === "test" && message.author.id === '303695993462063106') {
      sendDailyAnnouncementChannel(dsv, client, Discord);
      return;
    }
  }

  //Comandos con argumentos
  if (args.length) {
    if (cmd.infoCharacterAliases.includes(command)) {
      findCharacter(message, Discord, args, dsv)
      return;
    }

    if (cmd.infoWeaponAliases.includes(command)) {
      findWeapon(message, Discord, args)
      return;
    }

    if (cmd.infoArtifactAliases.includes(command)) {
      findArtifact(message, Discord, args)
      return;
    }

    if (cmd.infoTalentAliases.includes(command)) {
      findTalents(message, Discord, args)
      return;
    }

    if (cmd.infoConstellationsAliases.includes(command)) {
      findConstelations(message, Discord, args)
      return;
    }

    if (cmd.infoTalentMaterialAliases.includes(command)) {
      findTalentMaterials(message, Discord, args)
      return;
    }

    if (cmd.infoWeaponMaterialAliases.includes(command)) {
      findWeaponMaterials(message, Discord, args)
      return;
    }

    if (cmd.reminderAliases.includes(command)) {
      remindToUser(message, args, dsv)
      return;
    }

    if (cmd.reminderRemoveAliases.includes(command)) {
      removeRemindToUser(message, args, dsv)
      return;
    }
  }

  message.channel.send("Unknown command, hehe te nandayo 👉👈")


});


client.once("ready", () => {
  console.log("Startup complete");
  client.user.setActivity("-g help | 😎");


  cron.schedule('0 4 * * *', () => {
    var fs = require('fs');
    var guilds = fs.readdirSync('./database/servers')
    guilds.forEach(g => {
      try {
        var adsv = require(`./database/servers/${g}`)
        sendDailyAnnouncementChannel(adsv, client, Discord);
      } catch (e) {
        console.log("Tuve error en :", g);
      }
    });

  }, {
    scheduled: true,
    timezone: "America/Sao_Paulo"

  });

});



client.login(token);