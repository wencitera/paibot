const hints = require("../messages/message.json")
const { allTalentTypes, allWeaponTypes } = require("../database/db");
const { announcementEmbed} = require("../embeds/dailyEmbed");
const {footerInfo} = require("../config.json")


function getStringDay() {
  var day = new Date().getDay();
  switch (day) {
    case 0:
      day = "Sunday"
      break;
    case 1:
      day = "Monday"
      break;
    case 2:
      day = "Tuesday"
      break;
    case 3:
      day = "Wednesday"
      break;
    case 4:
      day = "Thursday"
      break;
    case 5:
      day = "Friday"
      break;
    case 6:
      day = "Saturday"
      break;
  }
  return day;
}

function getTodayTalent() {
  var todayTalent = [];
  var day = getStringDay();

  allTalentTypes.forEach(talent => {
    var ttype = require(`../database/db/talentmaterialtypes/${talent}`)
    if (ttype.day.includes(day)) {
      todayTalent.push(ttype['name'])
    }
  });



  return todayTalent;
}

function getTodayWeapon() {
  var todayWeapon = [];
  var day = getStringDay();
  allWeaponTypes.forEach(weapon => {
    var ttype = require(`../database/db/weaponmaterialtypes/${weapon}`)
    if (ttype.day.includes(day)) {
      todayWeapon.push(ttype['name'])
    }
  });
  return todayWeapon;
}

module.exports = {
  sendTodayAnnouncement(message, dsv, client, Discord) {
    if (dsv.announcementChannel !== '') announcementEmbed(client, Discord, dsv, getStringDay(), getTodayTalent(), getTodayWeapon());
    else message.channel.send(hints.missingAnnouncementChannel)
  },

  sendDailyAnnouncementChannel: async function sendDailyAnnouncementChannel(dsv, client, Discord) {

    var todayTalent = getTodayTalent();
    var todayWeapon = getTodayWeapon();
    var day = getStringDay();
    var ausers = dsv.users;
    for (var j = 0; j < ausers.length; j++) {
      var daily = [];
      var listaR = ausers[j].remind;
      await client.users.fetch(ausers[j].id).then((user) => {
        todayTalent.forEach(t => {
          for (var i = 0; i < listaR.length; i++) {
            var str = listaR[i];
            if (str.includes(t))
              daily.push(`Book of ${str}`);
          }
        })
        todayWeapon.forEach(w => {
          for (var i = 0; i < listaR.length; i++) {
            var str = listaR[i];
            if (str.includes(w)) daily.push(`${str}`)
          }
        })

        if (daily.length > 0) {

          var privateEmbed = new Discord.MessageEmbed()
            .setTitle(`**Materials to farm on ${day}**`)
            .setDescription(daily)
            .setAuthor('Paimon here!', client.users.cache.get('795800208117727242').avatarURL())
            .setColor("#f7f68f")
            .setFooter(footerInfo)
          try {
            client.users.cache.get(user.id).send(privateEmbed)
            console.log("Exitoso en: ", client.users.cache.get(user.id).username)
          } catch
          {
            console.log("Error en:", client.users.cache.get(user.id).username)
          }

        }
        //if (daily.length > 0) privateEmbed(client, Discord, user.id, day, daily)
      }
      );

    }
    /*
    ausers.forEach((u) => {
      
      //if(daily.length > 0) privateEmbed(client,Discord,client.users.cache.get(u.id).id,day,daily)
    });
*/
    if (dsv.announcementChannel !== '') announcementEmbed(client,Discord,dsv,day,todayTalent,todayWeapon);
  }
}