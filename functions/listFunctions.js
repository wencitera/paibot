const config = require("../config.json")
const footerInfo = config.footerInfo
var fs = require('fs');
    
module.exports = {

    listCommands: function listCommands(message,client, Discord){

        var commands = [
            "`-g h/help`                       - View all commands",
            "`-g s/support` - Show the discord link for get support",
            "`-g d/donate` - Show the donate card",
            "",
            "**Info commands:**",
            "`-g ic/infocharacter Character`   - View character info",
            "`-g iw/infoweapon Weapon`         - View weapon info",
            "`-g ia/infoartifact <Artifact Set>`  - View set artifact",
            "",
            "`-g t/talents Character`          - View talents of Character",
            "`-g c/constellations Character`   - View constellations of Character",
            "`-g wm/weaponmaterial Weapon`     - View material of weapon",
            "`-g tm/talentmaterial Character`  - View material talents of Character",
            "",
            "`-g lc/listcharacters` - View List of characters",
            "`-g la/listartifacts` - View List of set artifacts",
            "`-g lw/listweapons` - View List of weapons",
            "",
            "**Config Commands:**",
            "`-g sac/setannouncementchannel`   - Set channel for daily materials to farm",
            "`-g rac/removeannouncementchannel`- Remove channel for daily materials to farm",
            "",
            "**Remind Commands:**",
            "*I can remember materials to upgrade Talents and Weapons for you!*",
            "`-g r/remind <Character or Weapon>`        - Add to your remind list",
            "`-g rr/remindremove <Character or Weapon>` - Remove from your remind list",
            "`-g rl/remindlist`                         - See your remind list"
        ]

        var listCommand = new Discord.MessageEmbed()
            .setColor("#3486eb")
            .setAuthor(client.users.cache.get('795800208117727242').username + " commands",client.users.cache.get('795800208117727242').avatarURL())
            .setDescription(commands)
            .setFooter(footerInfo);
        message.channel.send(listCommand)
    },

    listWeapons: function listWeapons(message, Discord){
        
        var weapons = fs.readdirSync('./database/db/weapons')
        var polearms = []
        var bows=[]
        var catalyzers = [] 
        var swords = []
        var claymores = []
        
        weapons.forEach(wp => {
            var w = require(`../database/db/weapons/${wp}`)
            switch(w.weapontype){
                case "Bow":
                    bows.push(w.name)
                    break;
                case "Polearm":
                    polearms.push(w.name)
                    break;
                case "Catalyst":
                    catalyzers.push(w.name)
                    break;
                case "Sword":
                    swords.push(w.name);
                    break;
                case "Claymore":
                    claymores.push(w.name);
                    break;
            }
        });

        var listWeapons = new Discord.MessageEmbed()
            .setColor("#3486eb")
            .setTitle('List of Weapons')
            .setDescription('Full list of weapons')
            .addField('Claymores', claymores)
            .addField('Bows', bows)
            .addField('Swords', swords)
            .addField('Catalysts', catalyzers)
            .addField('Polearm', polearms)
            .setFooter(footerInfo);
        message.channel.send(listWeapons)
    },


    listCharacters: function listCharacters(message, Discord){
        var characters = fs.readdirSync("./database/db/characters")

        var listCharact = [];
        characters.forEach(c =>{
            var ch = require(`../database/db/characters/${c}`)
            listCharact.push(ch.name)
        }
        )

        var listCharacters = new Discord.MessageEmbed()
            .setColor("#3486eb")
            .setTitle('List of Characters')
            .setDescription('Full list of charactes')
            .addField('Characters', listCharact)
            .setFooter(footerInfo);
        message.channel.send(listCharacters)
    },

    listSetArtifact: function listSetArtifact(message, Discord)
    {
        var setArtifacts = fs.readdirSync("./database/db/artifacts")

        var listSetArtifact = [];

        setArtifacts.forEach(a => {
            var ar = require(`../database/db/artifacts/${a}`)
            listSetArtifact.push(ar.name)
        })

        var listArtifacts = new Discord.MessageEmbed()
            .setColor("#3486eb")
            .setTitle('List of Artifacts sets')
            .setDescription('Full list of artifacts')
            .addField('Artifacts Sets', listSetArtifact)
            .setFooter(footerInfo);
        message.channel.send(listArtifacts)
    }
}